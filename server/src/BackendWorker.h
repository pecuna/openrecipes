/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BACKEND_WORKER
#define BACKEND_WORKER

#include "HelperDefs.h"

#include <QObject>
#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <functional>

class BackendWorker : public QThread {
	Q_OBJECT

	private:
		QMutex stopMutex;
		bool stop;

		static std::list<std::function<void (BackendWorker*)>> tasks;
		static QMutex tasksMutex;
		static QWaitCondition waitCond;

	public:
		BackendWorker();
		void run() override;

		static void appendTask(const std::function<void (BackendWorker*)> &task);
		static std::pair<bool, std::vector<QByteArray>> getIds(quint64 userId);
		static bool addOrUpdate(const QByteArray &id, const QByteArray &cipher, const QByteArray &nonce, quint64 userId);
		static bool deleteEntry(const QByteArray &id, quint64 userId);
		static GetChangedRes getChanged(quint64 timestamp, quint64 userId);
		static GetSyncKeyRes getSyncKey(const QByteArray &id);
		static bool setSyncKey(const QByteArray &id, const QByteArray &cipher, const QByteArray &nonce);

	public slots:
		void onClose();

	signals:
		void criticalError();
		void gotIds(const GetIdsRes &ids);
		void addedOrUpdated(bool succ);
		void deleted(bool succ);
		void gotChanged(const GetChangedRes &res);
		void gotSyncKey(const GetSyncKeyRes &res);
		void settedSyncKey(bool succ);
};

#endif //BACKEND_WORKER
