/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "TcpConnection.h"
#include "Async.h"
#include "DbItem.h"
#include "RequestParser.h"
#include "Request.h"

#include <FixedSizeNetworkPackage.h>
#include <InternalError.h>

#include <memory>
#include <QtEndian>
#include <ctime>
#include <cassert>

TcpConnection::TcpConnection(QObject *parent, QTcpSocket *socket, quint64 startupTime)
:
	QObject(parent),
	connection(new SecureServerConnection(this, socket)),
	startupTime(startupTime),
	loggedIn(false),
	requestInProgress(false)
{
	qDebug() << "New TcpConnection";

	connect(connection, &SecureServerConnection::disconnected, this, [this]() {
			qDebug() << "Connection closed, closing TcpConnection";
			deleteLater();
	});

	connect(connection, &SecureServerConnection::userLoggedIn, this, &TcpConnection::onUserLoggedIn);

	/*
	connect(socket, QOverload<QAbstractSocket::SocketError>::of(&QTcpSocket::error), this, [this,socket](QTcpSocket::SocketError socketError __attribute__((unused))) {
			qWarning() << socket->errorString();
			deleteLater();
	});
	*/

	connect(connection, &SecureServerConnection::cryptoEstablished, this, [this]() {
			RequestParser *rp = new RequestParser(this, connection);
			connect(rp, &RequestParser::newRequest, this, &TcpConnection::onNewRequest);
			connect(rp, &RequestParser::error, this, [this]() {
					qCritical() << "Parser error, closing connection";
					deleteLater();
			});
	});
}

TcpConnection::~TcpConnection() {
	if (loggedIn) loggedInUsers.erase(userId);
	emit closed();
}

void TcpConnection::onNewRequest(std::shared_ptr<Request> request) {
	qDebug() << "onNewRequest";

	if (requestInProgress) {
		qCritical() << "New request while old one is not finished, closing connecton";
		deleteLater();
		return;
	}

	if ((!loggedIn) &&
			request->getType() != Request::Type::GetSyncKey &&
			request->getType() != Request::Type::GetStartupTime &&
			request->getType() != Request::Type::GetProtocolVersion) {
		qDebug() << "Not authenticated user requested privileged action, closing connection";
		deleteLater();
		return;
	}

	try {
		switch(request->getType()) {
			case Request::Type::GetProtocolVersion:
				{
					qDebug() << "Requesting protocol version";
					QByteArray response;
					response.resize(8);
					*reinterpret_cast<quint64*>(response.data()) = qToBigEndian<quint64>(1u); //Protocol version
					connection->write(response);
				}
				return;
			case Request::Type::GetStartupTime:
				{
					qDebug() << "Requesting startup time";
					QByteArray response;
					response.resize(8);
					*reinterpret_cast<quint64*>(response.data()) = qToBigEndian<quint64>(startupTime);
					connection->write(response);
				}
				return;
			case Request::Type::GetIds:
				qDebug() << "Requesting ids";
				Async::getIds(this, userId);
				requestInProgress = true;
				return;
			case Request::Type::AddOrUpdate:
				{
					std::shared_ptr<RequestAddOrUpdate> r = std::dynamic_pointer_cast<RequestAddOrUpdate>(request);
					const RequestAddOrUpdate::CipherAndNonce can = r->getCipherAndNonce();
					qDebug() << "Requesting add or update";
					Async::addOrUpdate(this, r->getId(), can.cipher, can.nonce, userId);
					requestInProgress = true;
				}
				return;
			case Request::Type::Delete:
				{
					std::shared_ptr<RequestDelete> r = std::dynamic_pointer_cast<RequestDelete>(request);
					qDebug() << "Requesting delete";
					Async::deleteEntry(this, r->getId(), userId);
					requestInProgress = true;
				}
				return;
			case Request::Type::GetChanged:
				{
					std::shared_ptr<RequestGetChanged> r = std::dynamic_pointer_cast<RequestGetChanged>(request);
					qDebug() << "Requesting changed";
					Async::getChanged(this, r->getTimestamp(), userId);
					requestInProgress = true;
				}
				return;
			case Request::Type::Close:
				{
					const quint64 t = static_cast<quint64>(std::time(nullptr));
					QByteArray response;
					response.resize(8);
					*reinterpret_cast<quint64*>(response.data()) = qToBigEndian<quint64>(t);
					connect(connection, &SecureConnection::bytesWritten, this, &TcpConnection::deleteLater);
					connection->write(response);
				}
				return;
			case Request::Type::GetSyncKey:
				{
					std::shared_ptr<RequestGetSyncKey> r = std::dynamic_pointer_cast<RequestGetSyncKey>(request);
					qDebug() << "Requesting sync key";
					Async::getSyncKey(this, r->getId());
					requestInProgress = true;
				}
				return;
			case Request::Type::SetSyncKey:
				{
					std::shared_ptr<RequestSetSyncKey> r = std::dynamic_pointer_cast<RequestSetSyncKey>(request);
					const RequestSetSyncKey::CipherAndNonce can = r->getCipherAndNonce();
					qDebug() << "Setting sync key";
					Async::setSyncKey(this, r->getId(), can.cipher, can.nonce);
					requestInProgress = true;
				}
				return;
		}
	} catch (const InternalError &e) {
		qDebug() << "An error occured, closing connection";
		deleteLater();
		return;
	}
}

void TcpConnection::onUserLoggedIn(quint64 userId) {
	assert(loggedIn == false);

	if (loggedInUsers.count(userId)) {
		qDebug() << "User allready has an open connection, closing this connection";
		deleteLater();
		return;
	}
	loggedIn = true;
	this->userId = userId;
	loggedInUsers.insert(userId);
	qDebug() << "User with id " << userId << " loggend in";
	qDebug() << loggedInUsers.size() << " users logged in in total";
}

void TcpConnection::onGotIds(const GetIdsRes &res) {
	if (!res.first) {
		qCritical() << "Error while getting ids, closing connection";
		connect(connection, &SecureConnection::bytesWritten, this, &TcpConnection::deleteLater);
		connection->write(QByteArray("ER"));
		return;
	}

	const std::vector<QByteArray> &ids = res.second;
	requestInProgress = false;
	QByteArray response;
	response.resize(8);
	*reinterpret_cast<quint64*>(response.data()) = qToBigEndian<quint64>(ids.size());
	connection->write(response);
	for (const auto &id : ids) connection->write(id);
	qDebug() << "Sending " << ids.size() << " ids to user with userId " << userId;
}

void TcpConnection::onAddedOrUpdated(bool succ) {
	requestInProgress = false;
	const QByteArray response = succ ? "OK" : "ER";
	connection->write(response);
}

void TcpConnection::onDeleted(bool succ) {
	requestInProgress = false;
	const QByteArray response = succ ? "OK" : "ER";
	connection->write(response);
}

void TcpConnection::onGotChanged(const GetChangedRes &res) {
	if (!res.first) {
		qCritical() << "Error while getting ids, closing connection";
		connect(connection, &SecureConnection::bytesWritten, this, &TcpConnection::deleteLater);
		connection->write(QByteArray("ER"));
		return;
	}

	const std::vector<DbItem> &items = res.second;

	requestInProgress = false;
	QByteArray response;
	response.resize(8);
	*reinterpret_cast<quint64*>(response.data()) = qToBigEndian<quint64>(items.size());
	connection->write(response);

	for (const auto &i : items) {
		connection->write(i.id);
		response.resize(2 * 8);
		*reinterpret_cast<quint64*>(response.data()) = qToBigEndian<quint64>(i.cipher.size());
		*reinterpret_cast<quint64*>(response.data() + 8) = qToBigEndian<quint64>(i.nonce.size());
		connection->write(response);
		connection->write(i.cipher);
		connection->write(i.nonce);
	}
}

void TcpConnection::onSettedSyncKey(bool succ) {
	requestInProgress = false;
	const QByteArray response = succ ? "OK" : "ER";
	connection->write(response);
}

void TcpConnection::onGotSyncKey(const GetSyncKeyRes &res) {
	if (!res.first) {
		qCritical() << "Error while getting sync key, closing connection";
		connect(connection, &SecureConnection::bytesWritten, this, &TcpConnection::deleteLater);
		connection->write(QByteArray("ER"));
		return;
	}

	requestInProgress = false;
	QByteArray response;
	response.resize(2 * 8);
	*reinterpret_cast<quint64*>(response.data()) = qToBigEndian<quint64>(res.second.first.size());
	*reinterpret_cast<quint64*>(response.data() + 8) = qToBigEndian<quint64>(res.second.second.size());
	connection->write(response);
	connection->write(res.second.first);
	connection->write(res.second.second);
}

size_t TcpConnection::getNumberOfLoggedInUsers() {
	return loggedInUsers.size();
}

std::unordered_set<quint64> TcpConnection::loggedInUsers;
