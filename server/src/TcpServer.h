/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TCP_SERVER
#define TCP_SERVER

#include <QObject>

class QTcpServer;

class TcpServer : public QObject {
	Q_OBJECT

	public:
		struct Config {
			quint16 port;
		};

	private:
		QTcpServer *tcpServer;
		const time_t startupTime;
		bool closingGently;

	private slots:
		void onNewConnection();
		void onTcpConnectionClosed();

	public:
		TcpServer(const Config &conf);

	public slots:
		void onCloseGently();
		void onPrintInfo();

	signals:
		void closed();
};

#endif //TCP_SERVER
