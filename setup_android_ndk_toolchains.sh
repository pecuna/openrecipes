cd `dirname "${BASH_SOURCE[0]}"`
${ANDROID_NDK_ROOT}/build/tools/make_standalone_toolchain.py \
	--arch arm --api 27 --install-dir android-ndk-toolchain/arm
${ANDROID_NDK_ROOT}/build/tools/make_standalone_toolchain.py \
	--arch x86 --api 27 --install-dir android-ndk-toolchain/x86
