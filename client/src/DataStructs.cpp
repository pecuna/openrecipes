#include "DataStructs.h"
#include "SqlBackend.h"
#include "Ingredient.h"

bool RecipeData::operator==(const QByteArray &id) const {
	if (this->id != id) return false;
	if (name != SqlBackend::getNameForRecipe(id)) return false;
	if (image != SqlBackend::getImageForRecipe(id)) return false;
	if (portions != SqlBackend::getPortionsForRecipe(id)) return false;
	if (instruction != SqlBackend::getInstructionForRecipe(id)) return false;
	QList<Ingredient*> ingr = SqlBackend::getIngredientsForRecipe(id);
	if (static_cast<size_t>(ingr.size()) != ingredients.size()) return false;
	bool ret = true;
	for (const auto &i : ingredients) {
		bool inList = false;
		for (const auto &j : ingr) {
			if (i == j->getId()) {
				inList = true;
				ingr.removeOne(j);
				delete j;
				break;
			}
		}
		if (!inList) {
			ret = false;
			break;
		}
	}
	for (const auto &i : ingr) delete i;
	return ret;
}

bool IngredientData::operator==(int id) const {
	if (unit != SqlBackend::getUnitForIngredient(id)) return false;
	if (article != SqlBackend::getArticleForIngredient(id)) return false;
	if (count != SqlBackend::getCountForIngredient(id)) return false;
	return true;
}
	
