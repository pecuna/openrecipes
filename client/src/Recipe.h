/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RECIPE_H
#define RECIPE_H

#include "Ingredient.h"

#include <QString>
#include <QObject>
#include <QList>
#include <QQmlListProperty>
#include <memory>
#include <QByteArray>

class Recipe : public QObject {
	Q_OBJECT
	Q_PROPERTY(QByteArray id READ getId NOTIFY idChanged)
	Q_PROPERTY(QString name READ getName WRITE setName NOTIFY dataChanged)
	Q_PROPERTY(QString image READ getImage WRITE setImage NOTIFY dataChanged)
	Q_PROPERTY(QString smallImage READ getSmallImage NOTIFY dataChanged)
	Q_PROPERTY(bool hasImage READ hasImage NOTIFY dataChanged)
	Q_PROPERTY(unsigned int portions READ getPortions WRITE setPortions NOTIFY dataChanged)
	Q_PROPERTY(double tmpPortions READ getTmpPortions WRITE setTmpPortions NOTIFY tmpPortionsChanged)
	Q_PROPERTY(QQmlListProperty<Ingredient> ingredients READ getIngredientsQml NOTIFY dataChanged)
	Q_PROPERTY(bool hasInstruction READ hasInstruction NOTIFY dataChanged)
	Q_PROPERTY(QString instruction READ getInstruction WRITE setInstruction NOTIFY dataChanged)

	private:
		QByteArray id;
		quint64 tmpPortions;
		QList<Ingredient*> ingredients;

	public:
		Recipe(const QByteArray &id, QList<Ingredient*> ingredients);
		Recipe(const QString &xml, QObject *parent);
		QByteArray getId() const;
		bool hasImage() const;
		bool hasInstruction() const;
		QString getName() const;
		QString getImage(unsigned int height = 120) const;
		QString getSmallImage() const;
		unsigned int getPortions() const;
		double getTmpPortions() const;
		//QList<Ingredient*> getIngredients();
		QQmlListProperty<Ingredient> getIngredientsQml();
		int getIngredientsCount() const;
		Ingredient* getIngredientAt(int pos);
		Q_INVOKABLE void addIngredient() const;
		Q_INVOKABLE void deleteIngredient(Ingredient *ingredient) const;
		void addIngredient(double count, const QString &unit, const QString &article) const;
		QString getInstruction() const;
		//void setId(const QByteArray &id);
		void setName(const QString &name);
		void setImage(const QString &imageSrc);
		void setPortions(unsigned int portions);
		void setTmpPortions(double portions);
		//void setIngredients(const QVariantList &ingredients);
		void setInstruction(const QString &instruction);
		void delImage();
		Q_INVOKABLE void refreshIngredients();
		QString toXML() const;

	public slots:
		void onRecipeChanged(const QByteArray &id);
		void onRecipeIdChanged(const QByteArray &oldId, const QByteArray &newId);

	signals:
		void dataChanged();
		void idChanged();
		void tmpPortionsChanged();
};

#endif //RECIPE_H
