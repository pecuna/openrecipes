/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "SecureClientConnection.h"
#include "Globals.h"
#include "SqlBackend.h"

#include <QTcpSocket>
#include <QHostInfo>
#include <QUrl>

SecureClientConnection::SecureClientConnection(bool authSelf)
:
	SecureConnection(nullptr, new QTcpSocket, SecureConnection::Role::Client, authSelf)
{
	const std::pair<QHostAddress, quint16> hnp = getHostNameAndPort();
	socket->connectToHost(hnp.first, hnp.second);
	if (!socket->waitForConnected(5000)) {
		onError();
		throw IERROR(QString("Can't connect to server %1:%2").arg(hnp.first.toString()).arg(hnp.second));
	}
}

bool SecureClientConnection::verifyRemotePublicKey(const QByteArray &key __attribute__ ((unused)) ) {
	QByteArray compKey = Globals::getDefaultServerKey();
	try {
		if (SqlBackend::getUseCustomSyncServer()) compKey = SqlBackend::getCustomSyncServerKey();

	} catch (const SqlNoResult &e) {}
	return compKey == key;
}

std::pair<QHostAddress, quint16> SecureClientConnection::getHostNameAndPort() {
	QString hostName = Globals::getDefaultServerHostName();
	quint16 port = Globals::getDefaultServerPort();
	try {
		if (SqlBackend::getUseCustomSyncServer()) {
			hostName = SqlBackend::getCustomSyncServerAddr();
			port = SqlBackend::getCustomSyncServerPort();
		}
	} catch (const SqlNoResult &e) {}
	const QHostInfo hi = QHostInfo::fromName(hostName);
	if (hi.addresses().isEmpty()) throw IERROR(QString("Can't get ip address from host %1").arg(hostName));
	qDebug() << "Connecting to" << hi.addresses().front() << ":" << port;
	return std::make_pair(hi.addresses().front(), port);
}
