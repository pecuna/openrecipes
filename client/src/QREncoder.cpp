/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "QREncoder.h"

#ifndef Q_OS_WIN
#include <qrencode.h>
#endif
#include <QImage>
#include <cassert>

QREncoder::QREncoder()
:
	QQuickImageProvider(QQuickImageProvider::Image)
{}

QImage QREncoder::requestImage(const QString &id, QSize *size, const QSize &requestedSize) {
#ifndef Q_OS_WIN
	QRcode *qrcode = QRcode_encodeString(id.toStdString().c_str(), 0, QR_ECLEVEL_L, QR_MODE_8, 1);
	if (!qrcode) return QImage();

	const unsigned int imageWidth = std::min(requestedSize.height(), requestedSize.width());
	const unsigned int scaleFactor = imageWidth >= static_cast<unsigned int>(qrcode->width) ? static_cast<unsigned int>(static_cast<double>(imageWidth) / static_cast<double>(qrcode->width)) : 1;
	if (imageWidth >= static_cast<unsigned int>(qrcode->width)) {
		assert(scaleFactor * qrcode->width <= imageWidth);
		assert((scaleFactor + 1) * qrcode->width > imageWidth);
	}

	QImage image(qrcode->width * scaleFactor, qrcode->width * scaleFactor, QImage::Format_ARGB32);
	int row = 0;
	for (int col = 0; ; ++col) {
		if (col == qrcode->width) {
			++row;
			if (row == qrcode->width) break;
			col = 0;
		}
		for (unsigned int i = 0; i < scaleFactor; ++i) {
			for (unsigned int j = 0; j < scaleFactor; ++j) {
				image.setPixel(col * scaleFactor + i, row * scaleFactor + j, qrcode->data[row * qrcode->width + col] & 0x01 ? qRgb(0, 0, 0) : qRgb(255, 255, 255));
			}
		}
	}
	QRcode_free(qrcode);

	*size = image.size();
	return image;
#else
	Q_UNUSED(id);
	Q_UNUSED(size);
	Q_UNUSED(requestedSize);
	return QImage();
#endif
}
