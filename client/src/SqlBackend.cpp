/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "SqlBackend.h"
#include "Recipe.h"
#include "PRandom.h"
#include "EncryptedItem.h"
#include "Backend.h"

#include "InternalError.h"

#include <list>
#include <QStandardPaths>
#include <QDir>
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>
#include <QStringList>
#include <sodium.h>
#include <cstring>

QSqlDatabase SqlBackend::addDatabase(const QString &connectionName) {
	QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", connectionName);
	const QString dirPath = QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
	if (!QDir().mkpath(dirPath)) {
		throw IERROR("Can't create dir");
	}
	const QString dbPath = QDir::cleanPath(dirPath + QDir::separator() + "database.db");
	qDebug() << "Database Path: " << dbPath;
	db.setDatabaseName(dbPath);
	if (!db.open()) throw IERROR("Can't open database");
	QSqlQuery query(db);
	query.prepare("PRAGMA foreign_keys = ON;");
	tryExec(query);
	query.prepare("PRAGMA defer_foreign_keys = ON;");
	tryExec(query);
	return db;
}

void SqlBackend::init() {
	SqlBackendBase::addDatabase = addDatabase;
	assert(!transmitter);
	transmitter.reset(new SqlBackendTransmitter);
	try {
		const int dbSchema = getDbSchema();
		if (dbSchema != 1) throw IERROR("Can't handle db schema, please update recipes");
	} catch (const InternalError &e) {
		createDatabase();
	}
}

void SqlBackend::createDatabase() {
	qDebug() << "Populating database";

	QSqlQuery query = getQuery();
	query.prepare("CREATE TABLE settings( "
			"name TEXT PRIMARY KEY, "
			"value BLOB);");
	tryExec(query);
	query.prepare("CREATE TABLE recipes( "
			"id BLOB PRIMARY KEY, "
			"name TEXT, "
			"image TEXT, "
			"portions INTEGER DEFAULT 1, "
			"instruction TEXT, "
			"changed INTEGER NOT NULL);");
	tryExec(query);
	query.prepare("CREATE TABLE ingredients( "
			"id INTEGER PRIMARY KEY, "
			"idRecipe BLOB NOT NULL "
			"	REFERENCES recipes(id) "
			"	ON DELETE CASCADE "
			"	ON UPDATE CASCADE, "
			"count INTEGER, "
			"unit TEXT, "
			"article TEXT);");
	tryExec(query);

	QByteArray syncKey;
	syncKey.resize(crypto_secretbox_KEYBYTES);
	crypto_secretbox_keygen(reinterpret_cast<unsigned char*>(syncKey.data()));

	setSetting("dbSchema", 1);
	setSetting("useCustomSyncServer", 0);
}

QByteArray SqlBackend::getSyncKey() {
	return getSetting("syncKey").value<QByteArray>();
}

quint64 SqlBackend::getLastSyncTimestamp() {
	return getSetting("lastSyncTimestamp").value<quint64>();
}

bool SqlBackend::getUseCustomSyncServer() {
	return getSetting("useCustomSyncServer").toBool();
}

QString SqlBackend::getCustomSyncServerAddr() {
	return getSetting("syncServerAddr").toString();
}

quint16 SqlBackend::getCustomSyncServerPort() {
	return getSetting("syncServerPort").value<quint16>();
}

QByteArray SqlBackend::getCustomSyncServerKey() {
	return getSetting("syncServerKey").value<QByteArray>();
}

void SqlBackend::setUseCustomSyncServer(bool use) {
	setSetting("useCustomSyncServer", use);
	mutex.lock();
	emit transmitter->syncSettingsChanged();
	mutex.unlock();
}

void SqlBackend::setCustomSyncServerAddr(const QString &addr) {
	setSetting("syncServerAddr", addr);
	mutex.lock();
	emit transmitter->syncSettingsChanged();
	mutex.unlock();
}

void SqlBackend::setCustomSyncServerPort(quint16 port) {
	setSetting("syncServerPort", port);
	mutex.lock();
	emit transmitter->syncSettingsChanged();
	mutex.unlock();
}

void SqlBackend::setCustomSyncServerKey(const QByteArray &key) {
	setSetting("syncServerKey", key);
	mutex.lock();
	emit transmitter->syncSettingsChanged();
	mutex.unlock();
}

void SqlBackend::setupSync() {
	QByteArray syncKey;
	syncKey.resize(crypto_secretbox_KEYBYTES);
	crypto_secretbox_keygen(reinterpret_cast<unsigned char*>(syncKey.data()));
	SqlTransaction trans;
	QSqlQuery query = getQuery();
	query.prepare("INSERT INTO settings (name, value) "
			"VALUES (\"syncKey\", :syncKey), (\"lastSyncTimestamp\", :lastSyncTimestamp);");
	query.bindValue(":syncKey", syncKey);
	query.bindValue(":lastSyncTimestamp", quint64(0));
	tryExec(query);
	generateSyncPublicKeyPair();
	resetChanged(true);
	trans.commit();
	mutex.lock();
	emit transmitter->syncSettingsChanged();
	mutex.unlock();
}

void SqlBackend::setupSync(const QByteArray &symmetricKey, const QByteArray &publicKey, const QByteArray &secretKey) {
	SqlTransaction trans;
	QSqlQuery query = getQuery();
	query.prepare("INSERT INTO settings (name, value) "
			"VALUES (\"syncKey\", :syncKey), (\"lastSyncTimestamp\", :lastSyncTimestamp), (\"syncPublicKey\", :publicKey), (\"syncSecretKey\", :secretKey);");
	query.bindValue(":syncKey", symmetricKey);
	query.bindValue(":lastSyncTimestamp", quint64(0));
	query.bindValue(":publicKey", publicKey);
	query.bindValue(":secretKey", secretKey);
	tryExec(query);
	resetChanged(true);
	trans.commit();
	mutex.lock();
	emit transmitter->syncSettingsChanged();
	mutex.unlock();
}

void SqlBackend::removeSyncSettings() {
	QSqlQuery query = getQuery();
	query.prepare("DELETE FROM settings "
			"WHERE name = \"syncKey\" OR name = \"lastSyncTimestamp\" OR name = \"syncPublicKey\" OR name = \"syncSecretKey\";");
	tryExec(query);
	mutex.lock();
	emit transmitter->syncSettingsChanged();
	mutex.unlock();
}	

void SqlBackend::updateLastSyncTimestamp(quint64 timestamp) {
	QSqlQuery query = getQuery();
	query.prepare("UPDATE settings SET value = :ts WHERE name = \"lastSyncTimestamp\";");
	query.bindValue(":ts", timestamp);
	tryExec(query);
}

void SqlBackend::connectSignals(Recipe *recipe) {
	assert(transmitter);
	connect(transmitter.get(), &SqlBackendTransmitter::recipeChanged, recipe, &Recipe::onRecipeChanged);
	connect(transmitter.get(), &SqlBackendTransmitter::recipeIdChanged, recipe, &Recipe::onRecipeIdChanged);
}

void SqlBackend::connectSignals(Ingredient *ingredient) {
	assert(transmitter);
	connect(transmitter.get(), &SqlBackendTransmitter::ingredientChanged, ingredient, &Ingredient::onIngredientChanged);
}

void SqlBackend::connectSignals(Backend *backend) {
	assert(transmitter);
	connect(transmitter.get(), &SqlBackendTransmitter::recipesListChanged, backend, &Backend::onRecipesListChanged);
	connect(transmitter.get(), &SqlBackendTransmitter::recipeChanged, backend, &Backend::onRecipeChanged);
	connect(transmitter.get(), &SqlBackendTransmitter::syncSettingsChanged, backend, &Backend::onSyncSettingsChanged);
}

void SqlBackend::connectSignals(SqlTransaction *trans) {
	assert(transmitter);
	connect(trans, &SqlTransaction::rolledBack, transmitter.get(), []() {
			emit transmitter->recipesListChanged();
			emit transmitter->recipeChanged(QByteArray());
			emit transmitter->ingredientChanged(-1);
	});
	connect(trans, &SqlTransaction::committed, transmitter.get(), []() {
			emit transmitter->recipesListChanged();
	});
}

template<class T, class S>
T SqlBackend::getOneValue(Table table, const QString &valueName, const S &id) {
	QString tableName;
	switch (table) {
		case Table::Recipes:
			tableName = "recipes";
			break;
		case Table::Ingredients:
			tableName = "ingredients";
			break;
	}
	QSqlQuery query = getQuery();
	query.prepare(QString("SELECT %1 FROM %2 WHERE id = :id").arg(valueName).arg(tableName));
	query.bindValue(":id", id);
	tryExec(query);

	if (!query.next()) throw SQLNORESULT();
	return query.value(0).value<T>();
}

template<class T>
void SqlBackend::setOneValueRecipes(const QString &valueName, const T &value, const QByteArray &id) {
	QSqlQuery query = getQuery();
	query.prepare(QString("UPDATE recipes SET %1 = :value, changed = 1 WHERE id = :id;").arg(valueName));
	query.bindValue(":id", id);
	query.bindValue(":value", value);
	tryExec(query);
}

template<class T>
void SqlBackend::setOneValueIngredients(const QString &valueName, const T &value, int id) {
	QSqlQuery query = getQuery();
	query.prepare("UPDATE recipes SET changed = 1 WHERE id = :idRecipe;");
	query.bindValue(":idRecipe", getIdRecipeForIngredient(id));
	tryExec(query);
	query.prepare(QString("UPDATE ingredients SET %1 = :value WHERE id = :id;").arg(valueName));
	query.bindValue(":id", id);
	query.bindValue(":value", value);
	tryExec(query);
	return;
}

QList<Recipe*> SqlBackend::getRecipes(const QString &filter) {
	QSqlQuery query = getQuery();
	query.prepare("SELECT DISTINCT r.id FROM recipes r LEFT JOIN ingredients i ON r.id = i.idRecipe WHERE UPPER(r.name) LIKE UPPER(:filter) OR UPPER(i.article) LIKE UPPER(:filter) ORDER BY name COLLATE NOCASE;");
	query.bindValue(":filter", QString("%%1%").arg(filter));
	tryExec(query);

	QList<Recipe*> recipes;
	while (query.next()) {
		const QByteArray id = query.value(0).value<QByteArray>();
		const QList<Ingredient*> ingredients = getIngredientsForRecipe(id);
		Recipe *recipe = new Recipe(id, ingredients);
		connectSignals(recipe);
		recipes.push_back(recipe);
	}
	return recipes;
}

Recipe* SqlBackend::getRecipe(const QByteArray &id) {
	QSqlQuery query = getQuery();
	query.prepare("SELECT COUNT(*) FROM recipes WHERE id = :id;");
	query.bindValue(":id", id);
	tryExec(query);

	if (!query.next()) throw SQLNORESULT();
	if (query.value(0).toInt() == 0) throw SQLNORESULT();

	const QList<Ingredient*> ingredients = getIngredientsForRecipe(id);
	Recipe *recipe = new Recipe(id, ingredients);
	connectSignals(recipe);
	return recipe;
}

QList<Ingredient*> SqlBackend::getIngredientsForRecipe(const QByteArray &idRecipe) {
	QSqlQuery query = getQuery();
	query.prepare("SELECT id FROM ingredients WHERE idRecipe = :idRecipe;");
	query.bindValue(":idRecipe", idRecipe);
	tryExec(query);

	QList<Ingredient*> ingredients;
	while (query.next()) {
		const int id = query.value(0).toInt();
		Ingredient *ingredient = new Ingredient(id);
		connectSignals(ingredient);
		ingredients.push_back(ingredient);
	}
	return ingredients;
}

QString SqlBackend::getNameForRecipe(const QByteArray &id) {
	return getOneValue<QString, QByteArray>(Table::Recipes, "name", id);
}

QString SqlBackend::getInstructionForRecipe(const QByteArray &id) {
	return getOneValue<QString, QByteArray>(Table::Recipes, "instruction", id);
}

QString SqlBackend::getImageForRecipe(const QByteArray &id) {
	return getOneValue<QString, QByteArray>(Table::Recipes, "image", id);
}

quint64 SqlBackend::getPortionsForRecipe(const QByteArray &id) {
	return getOneValue<quint64, QByteArray>(Table::Recipes, "portions", id);
}

void SqlBackend::setNameForRecipe(const QByteArray &id, const QString &name) {
	setOneValueRecipes<QString>("name", name, id);
	mutex.lock();
	assert(transmitter);
	emit transmitter->recipeChanged(id);
	mutex.unlock();
}

void SqlBackend::setInstructionForRecipe(const QByteArray &id, const QString &instruction) {
	setOneValueRecipes<QString>("instruction", instruction, id);
	mutex.lock();
	assert(transmitter);
	emit transmitter->recipeChanged(id);
	mutex.unlock();
}

void SqlBackend::setImageForRecipe(const QByteArray &id, const QString &image) {
	setOneValueRecipes<QString>("image", image, id);
	mutex.lock();
	assert(transmitter);
	emit transmitter->recipeChanged(id);
	mutex.unlock();
}

void SqlBackend::setPortionsForRecipe(const QByteArray &id, quint64 portions) {
	setOneValueRecipes<quint64>("portions", portions, id);
	mutex.lock();
	assert(transmitter);
	emit transmitter->recipeChanged(id);
	mutex.unlock();
}

quint64 SqlBackend::getCountForIngredient(int id) {
	return getOneValue<quint64, int>(Table::Ingredients, "count", id);
}

QString SqlBackend::getUnitForIngredient(int id) {
	return getOneValue<QString, int>(Table::Ingredients, "unit", id);
}

QString SqlBackend::getArticleForIngredient(int id) {
	return getOneValue<QString, int>(Table::Ingredients, "article", id);
}

QByteArray SqlBackend::getIdRecipeForIngredient(int id) {
	return getOneValue<QByteArray, int>(Table::Ingredients, "idRecipe", id);
}

void SqlBackend::setCountForIngredient(int id, quint64 count) {
	setOneValueIngredients<quint64>("count", count, id);
	mutex.lock();
	assert(transmitter);
	emit transmitter->ingredientChanged(id);
	mutex.unlock();
}

void SqlBackend::setUnitForIngredient(int id, const QString &unit) {
	setOneValueIngredients<QString>("unit", unit, id);
	mutex.lock();
	assert(transmitter);
	emit transmitter->ingredientChanged(id);
	mutex.unlock();
}

void SqlBackend::setArticleForIngredient(int id, const QString &article) {
	setOneValueIngredients<QString>("article", article, id);
	mutex.lock();
	assert(transmitter);
	emit transmitter->ingredientChanged(id);
	mutex.unlock();
}

void SqlBackend::addIngredientToRecipe(const QByteArray &idRecipe, quint64 count, const QString &unit, const QString &article) {
	QSqlQuery query = getQuery();
	query.prepare("INSERT INTO ingredients (idRecipe, count, unit, article) "
			"VALUES (:idRecipe, :count, :unit, :article);");
	query.bindValue(":idRecipe", idRecipe);
	query.bindValue(":count", count);
	query.bindValue(":unit", unit);
	query.bindValue(":article", article);
	tryExec(query);
	query.prepare("UPDATE recipes SET changed = 1 WHERE id = :idRecipe;");
	query.bindValue(":idRecipe", idRecipe);
	tryExec(query);
	mutex.lock();
	assert(transmitter);
	emit transmitter->recipeChanged(idRecipe);
	mutex.unlock();
}

Recipe* SqlBackend::addEmptyRecipe() {
	const QByteArray id = PRandom::getId();
	QSqlQuery query = getQuery();
	query.prepare("INSERT INTO recipes (id, changed) "
			"VALUES (:id, 1);");
	query.bindValue(":id", id);
	query.bindValue(":changed", 1);
	tryExec(query);
	Recipe *r = new Recipe(id, QList<Ingredient*>());
	connectSignals(r);
	mutex.lock();
	assert(transmitter);
	emit transmitter->recipesListChanged();
	mutex.unlock();
	return r;
}

void SqlBackend::deleteRecipe(const QByteArray &id) {
	QSqlQuery query = getQuery();
	query.prepare("DELETE FROM recipes "
			"WHERE id = :id;");
	query.bindValue(":id", id);
	tryExec(query);
	mutex.lock();
	assert(transmitter);
	emit transmitter->recipesListChanged();
	mutex.unlock();
}

void SqlBackend::deleteIngredient(int id) {
	const QByteArray idRecipe = getIdRecipeForIngredient(id);
	QSqlQuery query = getQuery();
	query.prepare("UPDATE recipes SET changed = 1 WHERE id = :idRecipe;");
	query.bindValue(":idRecipe", idRecipe);
	tryExec(query);
	query.prepare("DELETE FROM ingredients WHERE id = :id;");
	query.bindValue(":id", id);
	tryExec(query);
	mutex.lock();
	assert(transmitter);
	emit transmitter->recipeChanged(idRecipe);
	mutex.unlock();
}

void SqlBackend::toDB(const EncryptedRecipe &r) {
	const RecipeData rd = r.getData();

	QSqlQuery query = getQuery();
	query.prepare("SELECT COUNT(*) FROM recipes WHERE id = :id;");
	query.bindValue(":id", rd.id);
	tryExec(query);
	if (!query.next()) throw SQLNORESULT();
	const bool isInDb = query.value(0).toBool();

	if (isInDb) {
		query.prepare("UPDATE recipes SET name = :name, image = :image, instruction = :instruction, portions = :portions, changed = 0 WHERE id = :id;");
	} else {
		query.prepare("INSERT INTO recipes (id, name, image, instruction, portions, changed) "
				"VALUES (:id, :name, :image, :instruction, :portions, 0);");
	}
	query.bindValue(":id", rd.id);
	query.bindValue(":name", rd.name);
	query.bindValue(":image", rd.image);
	query.bindValue(":instruction", rd.instruction);
	query.bindValue(":portions", rd.portions);
	tryExec(query);

	if (isInDb) {
		query.prepare("DELETE FROM ingredients WHERE idRecipe = :id;");
		query.bindValue(":id", rd.id);
		tryExec(query);
	}

	for (const auto &i : rd.ingredients) {
		query.prepare("INSERT INTO ingredients (idRecipe, unit, article, count) "
				"VALUES (:idRecipe, :unit, :article, :count);");
		query.bindValue(":idRecipe", rd.id);
		query.bindValue(":unit", i.unit);
		query.bindValue(":article", i.article);
		query.bindValue(":count", i.count);
		tryExec(query);
	}

	mutex.lock();
	assert(transmitter);
	if (isInDb) {
		emit transmitter->recipeChanged(rd.id);
	} else {
		emit transmitter->recipesListChanged();
	}
	mutex.unlock();
}

void SqlBackend::toDB(const EncryptedItem &ei) {
	switch (ei.getType()) {
		case EncryptedItem::Type::Recipe:
			toDB(EncryptedRecipe(ei));
			break;
	}
}

void SqlBackend::setSyncKey(const QByteArray &publicSyncKey, const QByteArray secretSyncKey) {
	//if (skd.publicKey.size() != crypto_sign_PUBLICKEYBYTES || skd.secretKey.size() != crypto_sign_SECRETKEYBYTES) throw IERROR("Invalid key sizes");
	QSqlQuery query = getQuery();
	query.prepare("INSERT INTO settings (name, value) VALUES "
			"(\"syncPublicKey\", :publicKey), (\"syncSecretKey\", :secretKey);");
	query.bindValue(":syncPublicKey", publicSyncKey);
	query.bindValue(":syncSecretKey", secretSyncKey);
	tryExec(query);
}

void SqlBackend::deleteOthersIfNotChanged(const std::vector<QByteArray> &ids) {
	QSqlQuery query = getQuery();
	
	query.prepare("SELECT id FROM recipes WHERE changed = 0;");
	tryExec(query);
	while(query.next()) {
		bool remove = true;
		const QByteArray idRecipe = query.value(0).value<QByteArray>();
		for (const auto &id : ids) {
			if (id == idRecipe) {
				remove = false;
				break;
			}
		}
		if (remove) deleteRecipe(idRecipe);
		mutex.lock();
		emit transmitter->recipesListChanged();
		mutex.unlock();
	}
}

std::forward_list<std::pair<QByteArray, SqlBackend::Table>> SqlBackend::getChangedIds() {
	QSqlQuery query = getQuery();
	std::forward_list<std::pair<QByteArray, Table>> ids;

	query.prepare("SELECT id FROM recipes WHERE changed != 0;");
	tryExec(query);
	while(query.next()) ids.push_front(std::make_pair(query.value(0).value<QByteArray>(), Table::Recipes));

	return ids;
}

std::forward_list<EncryptedItem> SqlBackend::getChangedEnc() {
	QSqlQuery query = getQuery();
	QSqlQuery query2 = getQuery();
	std::forward_list<EncryptedItem> items;
	query.prepare("SELECT id, name, image, instruction, portions FROM recipes "
			"WHERE changed != 0;");
	tryExec(query);
	while (query.next()) {
		RecipeData rd;
		rd.id = query.value(0).value<QByteArray>();
		rd.name = query.value(1).toString();
		rd.image = query.value(2).toString();
		rd.instruction = query.value(3).toString();
		rd.portions = query.value(4).toDouble();

		query2.prepare("SELECT unit, article, count FROM ingredients "
				"WHERE idRecipe = :id;");
		query2.bindValue(":id", rd.id);
		tryExec(query2);
		rd.ingredients.reserve(std::max(0, query2.size()));
		while (query2.next()) {
			rd.ingredients.emplace_back();
			rd.ingredients.back().unit = query2.value(0).toString();
			rd.ingredients.back().article = query2.value(1).toString();
			rd.ingredients.back().count = query2.value(2).value<quint64>();
		}

		items.push_front(EncryptedItem::fromRecipeData(rd));
	}

	return items;
}

std::forward_list<QByteArray> SqlBackend::getAllIds() {
	QSqlQuery query = getQuery();
	std::forward_list<QByteArray> ids;
	query.prepare("SELECT id FROM recipes;");
	tryExec(query);
	while(query.next()) ids.push_front(query.value(0).value<QByteArray>());
	return ids;
}

void SqlBackend::changeId(const QByteArray &id, Table table) {
	const QByteArray newId = PRandom::getId();
	setOneValueRecipes<QByteArray>("id", newId, id);
	assert(table == Table::Recipes);
	mutex.lock();
	assert(transmitter);
	emit transmitter->recipeIdChanged(id, newId);
	mutex.unlock();
}

void SqlBackend::resetChanged(bool value) {
	QSqlQuery query = getQuery();
	query.prepare("UPDATE recipes SET changed = :value;");
	query.bindValue(":value", value);
	tryExec(query);
}

std::unique_ptr<SqlBackendTransmitter> SqlBackend::transmitter;
QMutex SqlBackend::mutex;
