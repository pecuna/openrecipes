<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>Backend</name>
    <message>
        <location filename="../src/Backend.cpp" line="413"/>
        <source>Share recipe</source>
        <translation>Rezept teilen</translation>
    </message>
</context>
<context>
    <name>MainWindowDesktop</name>
    <message>
        <source>Recipes</source>
        <translation type="vanished">Rezepte</translation>
    </message>
    <message>
        <location filename="../qml/MainWindowDesktop.qml" line="40"/>
        <source>Update available</source>
        <translation>Update verfügbar</translation>
    </message>
    <message>
        <location filename="../qml/MainWindowDesktop.qml" line="47"/>
        <source>An update to version %1 is available. Do you wan&apos;t to install it and restart OpenRecipes? (Please make shure to save any unsaved changes beforhand.)</source>
        <translation>Ein Update auf Version %1 ist verfügbar. Wollen Sie es installieren und OpenRecipes neu starten? (Bitte speichern Sie vorher alle nicht gespeicherten Änderungen.)</translation>
    </message>
    <message>
        <location filename="../qml/MainWindowDesktop.qml" line="62"/>
        <source>Downloading update…</source>
        <translation>Update wird heruntergeladen...</translation>
    </message>
    <message>
        <location filename="../qml/MainWindowDesktop.qml" line="75"/>
        <source>Please wait while the update is downloaded.</source>
        <translation>Bitte warten Sie, während das Update heruntergeladen wird.</translation>
    </message>
    <message>
        <location filename="../qml/MainWindowDesktop.qml" line="93"/>
        <source>Update failed</source>
        <translation>Update fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../qml/MainWindowDesktop.qml" line="101"/>
        <source>The download of the update failed.</source>
        <translation>Das Herunterladen des Updates ist fehlgeschlagen.</translation>
    </message>
</context>
<context>
    <name>MainWindowMobile</name>
    <message>
        <source>Recipes</source>
        <translation type="vanished">Rezepte</translation>
    </message>
</context>
<context>
    <name>OpenImageDialog</name>
    <message>
        <location filename="../qml/OpenImageDialog.qml" line="22"/>
        <source>Open image</source>
        <translation>Bild öffnen</translation>
    </message>
    <message>
        <location filename="../qml/OpenImageDialog.qml" line="23"/>
        <source>Images</source>
        <translation>Bilder</translation>
    </message>
</context>
<context>
    <name>OpenRecipeDialog</name>
    <message>
        <location filename="../qml/OpenRecipeDialog.qml" line="22"/>
        <source>Open recipe</source>
        <translation>Rezept öffnen</translation>
    </message>
    <message>
        <location filename="../qml/OpenRecipeDialog.qml" line="23"/>
        <source>Recipes</source>
        <translation>Rezepte</translation>
    </message>
</context>
<context>
    <name>RecipeEditView</name>
    <message>
        <location filename="../qml/RecipeEditView.qml" line="72"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../qml/RecipeEditView.qml" line="101"/>
        <source>Open image</source>
        <translation>Bild öffnen</translation>
    </message>
    <message>
        <location filename="../qml/RecipeEditView.qml" line="110"/>
        <source>Delete image</source>
        <translation>Bild löschen</translation>
    </message>
    <message>
        <location filename="../qml/RecipeEditView.qml" line="41"/>
        <source>Edit recipe</source>
        <translation>Rezept bearbeiten</translation>
    </message>
    <message>
        <location filename="../qml/RecipeEditView.qml" line="66"/>
        <source>Name:</source>
        <translation>Name:</translation>
    </message>
    <message>
        <location filename="../qml/RecipeEditView.qml" line="78"/>
        <source>Image:</source>
        <translation>Bild:</translation>
    </message>
    <message>
        <location filename="../qml/RecipeEditView.qml" line="121"/>
        <source>Ingredients for</source>
        <translation>Zutaten für</translation>
    </message>
    <message>
        <location filename="../qml/RecipeEditView.qml" line="136"/>
        <source>portion(s):</source>
        <translation>Portion(en):</translation>
    </message>
    <message>
        <location filename="../qml/RecipeEditView.qml" line="153"/>
        <source>Count</source>
        <translation>Anzahl</translation>
    </message>
    <message>
        <location filename="../qml/RecipeEditView.qml" line="166"/>
        <source>Unit</source>
        <translation>Einheit</translation>
    </message>
    <message>
        <location filename="../qml/RecipeEditView.qml" line="173"/>
        <source>Ingredient</source>
        <translation>Zutat</translation>
    </message>
    <message>
        <location filename="../qml/RecipeEditView.qml" line="182"/>
        <source>Delet ingredient</source>
        <translation>Zutat löschen</translation>
    </message>
    <message>
        <location filename="../qml/RecipeEditView.qml" line="199"/>
        <source>Add ingredient</source>
        <translation>Zutat hinzufügen</translation>
    </message>
    <message>
        <location filename="../qml/RecipeEditView.qml" line="205"/>
        <source>Instruction:</source>
        <translation>Anleitung:</translation>
    </message>
    <message>
        <location filename="../qml/RecipeEditView.qml" line="215"/>
        <source>Instruction</source>
        <translation>Anleitung</translation>
    </message>
    <message>
        <location filename="../qml/RecipeEditView.qml" line="231"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../qml/RecipeEditView.qml" line="238"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
</context>
<context>
    <name>RecipeView</name>
    <message>
        <location filename="../qml/RecipeView.qml" line="65"/>
        <source>Realy delete recipe &quot;%1&quot;?</source>
        <translation>Rezept &quot;%1&quot; wirklich löschen?</translation>
    </message>
    <message>
        <location filename="../qml/RecipeView.qml" line="57"/>
        <source>Delete recipe?</source>
        <translation>Rezept löschen?</translation>
    </message>
    <message>
        <source>Share recipe?</source>
        <translation type="vanished">Rezept teilen?</translation>
    </message>
    <message>
        <source>Share this recipe with all people in the local network?</source>
        <translation type="vanished">Dieses Rezept mit allen Leuten im lokalen Netzwerk teilen?</translation>
    </message>
    <message>
        <source>Sharing recipe…</source>
        <translation type="vanished">Teile Rezept…</translation>
    </message>
    <message>
        <source>Close this dialog when sharing is finished.</source>
        <translation type="vanished">Schließe diesen Dialog, wenn das Teilen beendet ist.</translation>
    </message>
    <message>
        <source>Share via network</source>
        <translation type="vanished">Über&apos;s Netzwerk teilen</translation>
    </message>
    <message>
        <location filename="../qml/RecipeView.qml" line="34"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../qml/RecipeView.qml" line="41"/>
        <source>Can&apos;t save recipe.</source>
        <translation>Kann Rezept nicht speichern.</translation>
    </message>
    <message>
        <location filename="../qml/RecipeView.qml" line="92"/>
        <source>Share</source>
        <translation>Teilen</translation>
    </message>
    <message>
        <location filename="../qml/RecipeView.qml" line="102"/>
        <source>Save to file</source>
        <translation>In Datei speichern</translation>
    </message>
    <message>
        <location filename="../qml/RecipeView.qml" line="112"/>
        <source>Delete recipe</source>
        <translation>Rezept löschen</translation>
    </message>
    <message>
        <location filename="../qml/RecipeView.qml" line="122"/>
        <source>Edit recipe</source>
        <translation>Rezept bearbeiten</translation>
    </message>
    <message>
        <location filename="../qml/RecipeView.qml" line="152"/>
        <source>Ingredients for %1 portions:</source>
        <translation>Zutaten für %1 Portionen:</translation>
    </message>
    <message>
        <location filename="../qml/RecipeView.qml" line="157"/>
        <source>There aren&apos;t any ingredients for this recipe.</source>
        <translation>Für diese Rezept gibt es keine Zutaten.</translation>
    </message>
    <message>
        <location filename="../qml/RecipeView.qml" line="179"/>
        <source>Convert ingredients for portions:</source>
        <translation>Zutaten für Portionen umrechnen:</translation>
    </message>
    <message>
        <location filename="../qml/RecipeView.qml" line="193"/>
        <source>Instruction:</source>
        <translation>Anleitung:</translation>
    </message>
    <message numerus="yes">
        <location filename="../qml/RecipeView.qml" line="152"/>
        <source>Ingredients for %n portion(s):</source>
        <translation>
            <numerusform>Zutaten für eine Portion:</numerusform>
            <numerusform>Zutaten für %n Portionen:</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>RecipesListView</name>
    <message>
        <location filename="../qml/RecipesListView.qml" line="30"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../qml/RecipesListView.qml" line="37"/>
        <source>The import of the recipe failed.</source>
        <translation>Der Import des Rezeptes ist fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../qml/RecipesListView.qml" line="53"/>
        <source>New recipe</source>
        <translation>Neues Rezept</translation>
    </message>
    <message>
        <location filename="../qml/RecipesListView.qml" line="59"/>
        <source>Create new recipe</source>
        <translation>Neues Rezept erstellen</translation>
    </message>
    <message>
        <location filename="../qml/RecipesListView.qml" line="69"/>
        <source>Import recipe from file</source>
        <translation>Rezept aus Datei importieren</translation>
    </message>
    <message>
        <location filename="../qml/RecipesListView.qml" line="83"/>
        <source>Syncing failed</source>
        <translation>Synchronisieren fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../qml/RecipesListView.qml" line="91"/>
        <source>The synchronization failed.</source>
        <translation>Die Synchronisation ist fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../qml/RecipesListView.qml" line="101"/>
        <source>Stopping sync…</source>
        <translation>Stoppe Synchronisation…</translation>
    </message>
    <message>
        <location filename="../qml/RecipesListView.qml" line="109"/>
        <source>Please wait whyle stoping sync.</source>
        <translation>Bitte warte, während die Synchronisation gestoppt wird.</translation>
    </message>
    <message>
        <location filename="../qml/RecipesListView.qml" line="124"/>
        <source>Syncing data…</source>
        <translation>Synchronisiere Daten…</translation>
    </message>
    <message>
        <location filename="../qml/RecipesListView.qml" line="137"/>
        <source>Please wait while synchronization is in progress.</source>
        <translation>Bitte warte während die Synchronisation läuft.</translation>
    </message>
    <message>
        <location filename="../qml/RecipesListView.qml" line="175"/>
        <source>Recipes</source>
        <translation>Rezepte</translation>
    </message>
    <message>
        <location filename="../qml/RecipesListView.qml" line="188"/>
        <source>Synchronize with server</source>
        <translation>Mit Server synchronisieren</translation>
    </message>
    <message>
        <location filename="../qml/RecipesListView.qml" line="198"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/RecipesListView.qml" line="207"/>
        <source>Create your first recipe.</source>
        <translation>Erstelle dein erstes Rezept.</translation>
    </message>
    <message>
        <location filename="../qml/RecipesListView.qml" line="214"/>
        <source>No recipe found.</source>
        <translation>Kein Rezept gefunden.</translation>
    </message>
    <message>
        <location filename="../qml/RecipesListView.qml" line="229"/>
        <source>Search recipe…</source>
        <translation>Rezept suchen…</translation>
    </message>
    <message>
        <location filename="../qml/RecipesListView.qml" line="238"/>
        <source>Clear filter</source>
        <translation>Filter löschen</translation>
    </message>
    <message>
        <location filename="../qml/RecipesListView.qml" line="346"/>
        <source>Add new recipe</source>
        <translation>Neues Rezept hinzufügen</translation>
    </message>
    <message>
        <source>Search for shared recipe</source>
        <translation type="vanished">Suche geteilte Rezepte</translation>
    </message>
</context>
<context>
    <name>RecipesSearchView</name>
    <message>
        <location filename="../qml/RecipesSearchView.qml" line="32"/>
        <source>Download shared recipe</source>
        <translation>Geteiltes Rezept herunterladen</translation>
    </message>
    <message>
        <location filename="../qml/RecipesSearchView.qml" line="46"/>
        <source>Select a recipe to download:</source>
        <translation>Wähle ein Rezept zum Herunterladen aus:</translation>
    </message>
    <message>
        <location filename="../qml/RecipesSearchView.qml" line="103"/>
        <source>Searching…</source>
        <translation>Suche…</translation>
    </message>
    <message>
        <location filename="../qml/RecipesSearchView.qml" line="116"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
</context>
<context>
    <name>SaveRecipeDialog</name>
    <message>
        <location filename="../qml/SaveRecipeDialog.qml" line="22"/>
        <source>Save recipe</source>
        <translation>Rezept speichern</translation>
    </message>
    <message>
        <location filename="../qml/SaveRecipeDialog.qml" line="24"/>
        <source>Recipes</source>
        <translation>Rezepte</translation>
    </message>
</context>
<context>
    <name>SettingsView</name>
    <message>
        <location filename="../qml/SettingsView.qml" line="29"/>
        <source>Sending sync key…</source>
        <translation>Sende Sync-Schlüssel…</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="88"/>
        <source>Your sync key</source>
        <translation>Dein Sync-Schlüssel</translation>
    </message>
    <message>
        <source>Your key:</source>
        <translation type="vanished">Dein Schlüssel:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="166"/>
        <source>Enter sync key</source>
        <translation>Sync-Schlüssel eingeben</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="42"/>
        <source>Please wait while your sync key is send to the server.</source>
        <translation>Bitte warte, wärend dein Sync-Schlüssel zum Server gesendet wird.</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="67"/>
        <source>Sending sync key failed</source>
        <translation>Senden des Sync-Schlüssels fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="75"/>
        <source>Sending your sync key failed.</source>
        <translation>Senden deines Sync-Schlüssels ist fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="103"/>
        <source>Scan the QR Code with your other device:</source>
        <translation>Scanne den QR-Code mit deinem anderen Gerät:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="119"/>
        <location filename="../qml/SettingsView.qml" line="185"/>
        <source>If scaning the QR Code is not possible, you can also type in the key by hand:</source>
        <translation>Wenn es nicht möglich ist, den QR-Code zu scannen, kannst du ihn auch von Hand eingeben:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="135"/>
        <source>(Your sync key will be available on the server for %1:%2 minutes.)</source>
        <translation>(Dein Sync-Schlüssel wird auf dem Server für %1:%2 Minuten verfügbar sein.)</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="177"/>
        <source>Scan QR Code</source>
        <translation>Scanne QR-Code</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="185"/>
        <source>Enter the sync key from the device you wish to sync with:</source>
        <translation>Gebe den Sync-Schlüssel von dem Gerät ein, mit dem du synchronisieren willst:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="195"/>
        <location filename="../qml/SettingsView.qml" line="329"/>
        <source>Key</source>
        <translation>Schlüssel</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="211"/>
        <source>Receiving sync key…</source>
        <translation>Empfange Sync-Schlüssel…</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="224"/>
        <source>Please wait while your sync key is fetched from the server.</source>
        <translation>Bitte warte, währemd dein Sync-Schlüssel von Server abgerufen wird.</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="248"/>
        <source>Receiving sync key failed</source>
        <translation>Empfangen des Sync-Schlüssels fehlgeschlagen</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="256"/>
        <source>Receiving your sync key failed.</source>
        <translation>Empfangen deines Sync-Schlüssels ist fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="266"/>
        <source>Advanced sync settings</source>
        <translation>Erweiterte Synchronisationseinstellungen</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="273"/>
        <source>Use a custom sync server</source>
        <translation>Einen individuellen Sync-Server benutzen</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="279"/>
        <source>Sync server address:</source>
        <translation>Sync-Server Addresse:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="288"/>
        <source>domain.tld</source>
        <translation>domain.tld</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="296"/>
        <source>Sync server port:</source>
        <translation>Sync-Server Port:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="309"/>
        <source>1234</source>
        <translation>1234</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="318"/>
        <source>Sync server key:</source>
        <translation>Sync-Server Schlüssel:</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="342"/>
        <location filename="../qml/SettingsView.qml" line="466"/>
        <source>About %1</source>
        <translation>Über %1</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="357"/>
        <source>%1 %2</source>
        <translation>%1 %2</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="364"/>
        <source>Author: Johannes Schwab</source>
        <translation>Autor: Johannes Schwab</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="383"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="401"/>
        <source>Synchronization settings</source>
        <translation>Synchronisationseinstellungen</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="413"/>
        <source>Add device to sync with this one</source>
        <translation>Ein Gerät zum Synchronisieren mit diesem Hinzufügen</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="423"/>
        <source>Sync with another device</source>
        <translation>Mit einem anderen Gerät synchronisieren</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="434"/>
        <source>Reset sync</source>
        <translation>Synchronisation zurücksetzen</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="444"/>
        <source>Advanced Settings</source>
        <translation>Erweiterte Einstellungen</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="452"/>
        <source>You are currently using the custom sync server &quot;%1:%2&quot;</source>
        <translation>Du benutzt im Moment der individuellen Sync-Server &quot;%1:%2&quot;</translation>
    </message>
    <message>
        <location filename="../qml/SettingsView.qml" line="478"/>
        <source>Back</source>
        <translation>Zurück</translation>
    </message>
</context>
<context>
    <name>SynchronizeAsync</name>
    <message>
        <location filename="../src/SynchronizeAsync.cpp" line="203"/>
        <source> (local version)</source>
        <translation> (lokale Version)</translation>
    </message>
</context>
<context>
    <name>UnsupportedVersionWindow</name>
    <message>
        <location filename="../qml/UnsupportedVersionWindow.qml" line="35"/>
        <source>Your version of Qt is not supported. Please update to Qt %1 or higher and try again.</source>
        <translation>Deine Qt-Version wird nicht unterstützt. Bitte aktualisiere Qt auf Version %1 oder höher.</translation>
    </message>
</context>
</TS>
