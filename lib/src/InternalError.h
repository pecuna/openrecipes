/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef INTERNAL_ERROR
#define INTERNAL_ERROR

#include <exception>
#include <cstdio>
#include <QDebug>

/*!
 * \brief Class used for internal error handling.
 */
class InternalError : public std::exception {
	/*!
	 * \brief Error description.
	 */
	char *errStr;

	public:
		/*!
		 * \brief Constructor.
		 * \param errStr error description
		 * \param file file where the error was thrown
		 * \param line line where the error was thrown
		 *
		 * IERROR(str) is an convenience macro that sets file and line.
		 */
		InternalError(
				const char *errStr,
				const char *file,
				const unsigned int line)
		: errStr(new char[strlen(errStr) + strlen(file) + 40]) {
			snprintf(
					this->errStr,
					strlen(errStr) + strlen(file) + 40,
					"%s (in file %s, line %u)",
					errStr,
					file,
					line
			);
			qDebug() << this->errStr;
		};

		/*!
		 * \brief Constructor.
		 * \param errStr error description
		 * \param file file where the error was thrown
		 * \param line line where the error was thrown
		 *
		 * IERROR(str) is an convenience macro that sets file and line.
		 */
		InternalError(
				const QString &errStr,
				const char *file,
				const unsigned int line)
		: InternalError(errStr.toStdString().c_str(), file, line) {};

		/*!
		 * \brief Destructor.
		 */
		~InternalError() override {
			delete[] errStr;
		};

		/*!
		 * \brief Get the error description.
		 */
		const char* what() const noexcept override {
			return errStr;
		};
};

#define IERROR(str) InternalError(str, __FILE__, __LINE__)

#endif //INTERNAL_ERROR
