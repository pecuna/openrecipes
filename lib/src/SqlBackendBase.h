/*
 * OpenRecipes
 * Copyright (C) 2018 Johannes Schwab
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SQL_BACKEND_BASE_H
#define SQL_BACKEND_BASE_H

#include "InternalError.h"

#include <QString>
#include <QObject>
#include <functional>
#include <QSqlDatabase>
#include <cassert>

class QThread;
class QSqlQuery;
class QByteArray;
class QVariant;
class SqlTransaction;

/*!
 * \brief Base class for Sql backend.
 */
class SqlBackendBase : public QObject {
	Q_OBJECT

	private:
		/*!
		 * \brief Get a database connection.
		 * 
		 * For each thread a different connection is returned.
		 */
		static QSqlDatabase getDatabase();

	protected:
		/*!
		 * \brief Get a database querry.
		 *
		 * For each thread, a different database connection is used.
		 */
		static QSqlQuery getQuery();

		/*!
		 * \brief Execute the given query.
		 * \exception InternalError when query can't be executed.
		 */
		static void tryExec(QSqlQuery &query);


		/*!
		 * \brief Generate a new public/secret key pair for sync and write it to the db.
		 * \exception InternalError
		 */
		static void generateSyncPublicKeyPair();

		/*!
		 * \brief Get setting from database.
		 * \param name setting to get
		 * \exception InternalError when the query can't be executed
		 * \exception SqlNoResult if there is no such setting in the db or it is null
		 */
		static QVariant getSetting(const QString &name);

		/*!
		 * \brief Insert or update setting in db.
		 * \param name setting to insert/update
		 * \param value new value
		 * \exception InternalError
		 * \exception SqlNoResult
		 */
		static void setSetting(const QString &name, const QVariant &value);

		/*!
		 * \brief Get current db schema.
		 * \exception InternalError
		 * \exception SqlNoResult
		 */
		static int getDbSchema();

		/*!
		 * \brief This function to called whenever a new database connection is opened.
		 *
		 * The default value does nothing usefull. Must be replaced with a function that adds a database connection with name "connectionName" by calling QSqlDatabase::addDatabase() and opens it.
		 */
		static std::function<QSqlDatabase (const QString &connectionName)> addDatabase;

	public:
		/*!
		 * \brief Get sync secret key.
		 * \exception InternalError
		 * \exception SqlNoResult
		 */
		static QByteArray getSyncSecretKey();

		/*!
		 * \brief Get sync public key.
		 * \exception InternalError
		 * \exception SqlNoResult
		 */
		static QByteArray getSyncPublicKey();

	/*!
	 * \brief SqlTransaction needs access to private and protected members and QObject doesn't support nested classes.
	 */
	friend SqlTransaction;
};

/*!
 * \brief A lockguard like class for a sql transaction.
 */
class SqlTransaction : public QObject {
	Q_OBJECT

	private:
		/*!
		 * \brief Wether or not the transaction is allready commited.
		 */
		bool commited;

	public:
		/*!
		 * \brief Starts a new transaction.
		 */
		SqlTransaction() : commited(false) {
			if (!SqlBackendBase::getDatabase().transaction()) throw IERROR("Can't start transaction");;
			qDebug() << "New sql transaction";
		};

		/*!
		 * \brief Rolls back the transaction if it is not commited.
		 */
		~SqlTransaction() {
				if (!commited) {
				const bool ok = SqlBackendBase::getDatabase().rollback();
				assert(ok);
				qDebug() << "Rollback sql transaction";
				emit rolledBack();
			}
		}

		/*!
		 * \brief Commits the transaction.
		 */
		void commit() {
			assert(!commited);
			commited = true;
			if (!SqlBackendBase::getDatabase().commit()) throw IERROR("Can't commit");
			qDebug() << "Commit sql transaction";
			emit committed();
		}

	signals:
		/*!
		 * \brief Emitted if the transaction is rolled back.
		 */
		void rolledBack();

		/*!
		 * \brief Emitted if the transaction is committed.
		 */
		void committed();
};

/*!
 * \brief Error class to signal that the requested data wasn't in the db.
 */
class SqlNoResult : public InternalError {
	public:
		/*!
		 * \brief Constructor.
		 * \param file file where the error was thrown
		 * \param line line where the error was thrown
		 *
		 * SQLNORESULT() is an convenience macro that sets file and line.
		 */
		explicit SqlNoResult(
				const char *file,
				const unsigned int line)
		: InternalError("No Sql Result", file, line) {};
};

#define SQLNORESULT() SqlNoResult(__FILE__, __LINE__)

#endif //SQL_BACKEND_BASE_H
